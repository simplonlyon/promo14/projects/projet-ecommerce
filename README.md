# Projet e-commerce

L'objectif de ce projet sera de réaliser un site de vente en ligne par groupes de 4 personnes en 4 semaines et demi.
A votre équipe de trouver un sujet innovant et motivant (Que vous seriez fier(e) de présenter à vos futurs entretiens).
Le thème du site est "libre" (ça veut dire que si le thème est éthiquement limite, il sera refusé)
Les contraintes sont les suivantes : 
### Conception
* Réalisation de diagrammes de Use Case
* Réalisation d'un diagramme de classe
* Réalisation de maquettes (wireframe) mobile first
### Technique
* Utilisation Express.js et TypeORM pour le back
* Utilisation de React pour le front (avec Typescript et pourquoi pas Redux/RTK Query)
* Application responsive (Antd/Material-ui/Bootstrap/bootswatch/etc.)
* Utilisation de git/gitlab pour le travail en groupe
### Organisation Agile
* Faire au moins 4 User stories sur le modèle de celles proposées
* 4 sprints (un par semaine) avec les fonctionnalités à livrer
    * Au début de chaque sprint, définir le livrable attendu, les tâches à réaliser pour puis faire un mail à votre groupe de review indiquant les fonctionnalités priorisées (buy a feature) à livrer/presenter en fin de semaine
    * En début de chaque après midi, faire un daily scrum de 15 minutes max pour échanger sur vos soucis
    * A la fin de chaque sprint, faire une review avec le groupe reviewer (20 minutes max). Garder une trace de la review dans une issue gitlab avec des cases à cocher.
    * Terminer la semaine par une retrospective d'équipe avec un formateur
* Utiliser la partie Issues et les Boards du projet gitlab pour définir vos tâches et votre kanban

Vous réaliserez également un README avec vos différents diagrammes commentés et une petite présentation du projet.


## User Stories obligatoires

1. En tant qu'utilisateur.ice, je veux pouvoir visualiser la liste des produits filtrés pour faciliter mon choix
2. En tant qu'utilisateur.ice, je veux pouvoir ajouter des produits à mon panier pour passer rapidement ma commande
3. En tant que visiteur.euse, je veux pouvoir créer mon compte pour accéder à des fonctionnalités supplémentaires du site
4. En tant qu'administrateur.ice, je veux pouvoir gérer les produits disponibles afin de modifier le catalogue
Rappel : vous pouvez découper les user story (culture du backlog)

## Les Groupes
1. Quentin, Andy, Thanmy, Amira
2. Fanny, Henda, Christopher, Hedwig
3. Kevin, Jehane, Ling, Daniela
4. Kamel, Lelia, Riad, Nasrine
